# Copyright (C) 2017 Morgan W. Estes
# This file is distributed under the same license as the Copyright Autoupdater package.
msgid ""
msgstr ""
"Project-Id-Version: Copyright Autoupdater 1.0.0\n"
"Report-Msgid-Bugs-To: "
"https://wordpress.org/support/plugin/copyright-autoupdater\n"
"POT-Creation-Date: 2017-01-05 03:54:58+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2017-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"X-Generator: grunt-wp-i18n 0.5.4\n"
"X-Poedit-KeywordsList: "
"__;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_"
"attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Country: United States\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-Bookmarks: \n"
"X-Textdomain-Support: yes\n"

#: inc/template-tags.php:28
#. translators: 1: site name, 2: starting year, 3: current year
msgid "Content copyright %1$s &copy;&thinsp;%2$s&ndash;%3$s"
msgstr ""

#: inc/utilities.php:50
msgid "No posts found."
msgstr ""

#. Plugin Name of the plugin/theme
msgid "Copyright Autoupdater"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "https://gitlab.com/morganestes/copyright-autoupdater"
msgstr ""

#. Description of the plugin/theme
msgid "Automatically generates and updates your site's copyright text."
msgstr ""

#. Author of the plugin/theme
msgid "Morgan W. Estes"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://morganestes.com/"
msgstr ""